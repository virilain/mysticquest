# MysticQuest

## Description
MysticQuest is a project that was created by two friends and me while working
in a programming class that we took at the local community collage while in
high school. This is a continuation of that project a little while after its
inception. 

## How to play
The interface at the moment is a basic input with N, S, E, W, and Q are the only
options representing North, South, East, West and Quit. If an unrecognized
input is entered, a help menu will come up. 

## Notes
- As stated above, this is constantly getting updated and upgraded so this
README is going to updated as much as I remember to, but it might not always be
the most acurate.